#include<iostream>
using namespace std;

int N;
int A[1001];
int DP[1001];

int main(){
    cin>>N;
    for(int i=0;i<N;i++){
        cin>>A[i];
    }
    for(int i=0;i<N;i++){
        DP[i]=1;
    }
    for(int i=0;i<N;i++){
        for(int j=i-1;j>=0;j--){
            if(A[j]<A[i]&&DP[j]>=DP[i]){
                DP[i]=DP[j]+1;
            }
        }
    }
    int max=-1;
    for(int i=0;i<N;i++){
        if(max<DP[i]) max=DP[i];
    }
    cout<<max;
}