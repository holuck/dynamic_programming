#include<iostream>
#include<algorithm>
using namespace std;

int p[1001];
int DP[1001];

int main(){
    int N;
    cin>>N;
    for(int i=1;i<=N;i++){
        int num;
        cin>>num;
        p[i]=num;
    }
    DP[0]=0;
    for(int i=1;i<=N;i++){
        for(int j=1;j<=i;j++){
            DP[i]=max(DP[i],DP[i-j]+p[j]);
        }
    }
    cout<<DP[N];
}